<?php

namespace Bss\HelloWorld\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Result\PageFactory;

class Add extends \Magento\Framework\View\Element\Template
{
    protected $templateContext;
    protected $_pageFactory;

    /**
     * Add constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getFormAction()
    {
        return 'add';
    }
}
