<?php

namespace Bss\HelloWorld\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\App\Request\Http;

class Show extends \Magento\Framework\View\Element\Template
{
    protected $templateContext;
    protected $_pageFactory;
    protected $request;

    /**
     * Add constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        array $data = [],
        \Magento\Framework\App\Request\Http $request
    )

    {
        $this->request = $request;
        parent::__construct($context, $data);
    }
    /**
     * @return string
     */
    public function getFormAction()
    {
        return 'add';
    }

    public function getShowInfo()
    {
        $id = $this->request->getParam('id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('internship'); //gives table name with prefix

        $query = $connection->select()
            ->from($tableName, ['id', 'name', 'avatar', 'dob', 'description'])
            ->where('id = ?', $id);

        $fetchData = $connection->fetchAll($query);
        foreach ($fetchData as $i) {
            echo $i['id'] . "<br>";
            echo $i['name'] . "<br>";
            echo $i['avatar'] . "<br>";
            echo $i['dob'] . "<br>";
            echo $i['description'] . "<br>";
        }
    }
}

