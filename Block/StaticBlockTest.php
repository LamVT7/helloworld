<?php
declare(strict_types=1);

namespace Bss\HelloWorld\Block;

use Magento\Framework\View\Element\Template as CoreTemplate;
use Magento\Framework\UrlInterface;

/**
 * Class FooterLink, add custom link to footer for easy management
 *
 * Bss\HelloWorld\Block
 */
class StaticBlockTest extends CoreTemplate
{
    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * FooterLink constructor.
     *
     * @param CoreTemplate\Context $context
     * @param UrlInterface $url
     * @param array $data
     */
    public function __construct(
        CoreTemplate\Context $context,
        UrlInterface $url,
        array $data = []
    ) {
        $this->url = $url;
        CoreTemplate::__construct($context, $data);
    }

    /**
     *  Get action url
     *
     * @param string $action
     * @return string
     */
    public function getStaticBlockTest(string $action): string
    {
        if (!$action || $action === '' || $action === null) {
            return $this->url->getUrl('defaultNoRoute');
        }
        return $this->url->getUrl($action);
    }
}
