<?php

namespace Bss\HelloWorld\Block;

use Bss\HelloWorld\Model\InternDataFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Internship
 * @package Bss\HelloWorld\Block
 */
class Internship extends Template
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;
    protected $_interndataFactory;
    protected $serializer;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param InternDataFactory $postFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        InternDataFactory $interndataFactory,
        SerializerInterface $serializer,
        array $data = []
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_interndataFactory = $interndataFactory;
        $this->serializer = $serializer;

        parent::__construct($context, $data);
    }

    /**
     * get data from database
     */
    public function getTable()
    {
        $interndata = $this->_interndataFactory->create();
        $collection = $interndata->getCollection();
        return $collection;
    }
}

