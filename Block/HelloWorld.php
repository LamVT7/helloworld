<?php
namespace Bss\HelloWorld\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template as CoreTemplate;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Bss\HelloWorld\Helper\ConfigData;

/**
 * Class HelloWorld, block contain function that prepare data of the module
 *
 * Bss\HelloWorld\Block
 */
class HelloWorld extends CoreTemplate
{
    public const CONFIG_DATA_NAME = 'name';
    public const CONFIG_DATA_IMAGE = 'image';
    public const CONFIG_DATA_AGE = 'age';
    public const CONFIG_DATA_DOB = 'dob';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ConfigData
     */
    protected $configData;

    /**
     * HelloWorld constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigData $config
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        ConfigData $config
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configData = $config;
        parent::__construct(
            $context
        );
    }

    /**
     * Get default HelloWorld text
     *
     * @return mixed|string
     */
    public function getHelloWorldTxt()
    {
        $text = $this->getText();
        if (!isset($text)) {
            $text = 'Hello World';
        } else {
            $text = 'Hello ' . $text;
        }
        return $text;
    }

    /**
     * Function get config text
     *
     * @return mixed
     */
    public function getText()
    {
        return $this->scopeConfig
            ->getValue(
                'helloworld/general/text_content',
                ScopeInterface::SCOPE_STORE
            );
    }

    /**
     * Function that return page header
     *
     * @return string
     */
    public function getHeaderText(): string
    {
        /* Never forget to put in translate */
        return __("Developer Information");
    }

    /**
     * Function that return name
     *
     * @return string
     */
    public function getName(): string
    {
        return __($this->configData->getConfigInfo(self::CONFIG_DATA_NAME));
    }

    /**
     * Function that return name
     *
     * @return string|mixed
     */
    public function getImage()
    {
        return __($this->configData->getConfigInfo(self::CONFIG_DATA_IMAGE));
    }

    /**
     * Function that return name
     *
     * @return string
     */
    public function getAge(): string
    {
        return __($this->configData->getConfigInfo(self::CONFIG_DATA_AGE));
    }

    /**
     * Function that return name
     *
     * @return string
     */
    public function getDob(): string
    {
        return __($this->configData->getConfigInfo(self::CONFIG_DATA_DOB));
    }
}
