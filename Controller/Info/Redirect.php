<?php

namespace Bss\HelloWorld\Controller\Info;

use Bss\HelloWorld\Controller\AbstractController;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Redirect
 *
 * Bss\HelloWorld\Controller\Info
 */
class Redirect extends AbstractController
{
    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * Redirect constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResultFactory $resultFactory
    ) {
        $this->resultFactory = $resultFactory;
        parent::__construct(
            $context,
            $resultPageFactory
        );
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $resultFactory = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        /* Set redirect path */
        return $resultFactory->setPath('cms/index/index');
    }
}
