<?php

namespace Bss\HelloWorld\Controller\Info;

use Bss\HelloWorld\Controller\AbstractController;

/**
 * Class Index indicate controller index action
 *
 * Bss\HelloWorld\Controller\Index
 */
class Index extends AbstractController
{

}
