<?php

namespace Bss\HelloWorld\Controller\Info;

use Bss\HelloWorld\Helper\ConfigData;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Json that determine return data
 */
class Json extends Action
{
    public const CONFIG_DATA_NAME = 'name';
    public const CONFIG_DATA_IMAGE = 'image';
    public const CONFIG_DATA_AGE = 'age';
    public const CONFIG_DATA_DOB = 'dob';

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * @var ConfigData
     */
    protected $configData;

    /**
     * Json constructor.
     *
     * @param Context $context
     * @param SerializerInterface $serializer
     * @param JsonFactory $jsonFactory
     * @param ConfigData $configData
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer,
        JsonFactory $jsonFactory,
        ConfigData $configData
    ) {
        $this->serializer = $serializer;
        $this->jsonFactory = $jsonFactory;
        $this->configData = $configData;
        parent::__construct($context);
    }

    /**
     * Return Json Data as requested
     *
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        /* Create result json */
        $resultJson = $this->jsonFactory->create();

        /* Check if data exist, else return default data; */
        $name = ($this->configData->getConfigInfo(self::CONFIG_DATA_NAME)) ?: "Default Name";
        $image = ($this->configData->getConfigInfo(self::CONFIG_DATA_IMAGE)) ?: "No Image";
        $age = ($this->configData->getConfigInfo(self::CONFIG_DATA_AGE)) ?: '0';
        $dob = ($this->configData->getConfigInfo(self::CONFIG_DATA_DOB)) ?: "No Date";

        $data = [
            "name" => $name,
            "image" => $image,
            "age" => $age,
            "dob" => $dob
        ];

        /* Serialize Array to Json*/
        $data = $this->serializer->serialize($data);
        return $resultJson->setData($data);
    }
}
