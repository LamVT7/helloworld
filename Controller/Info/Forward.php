<?php
/* New Magento 2 standard */
declare(strict_types=1);

namespace Bss\HelloWorld\Controller\Info;

use Bss\HelloWorld\Controller\AbstractController;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Forward declare forward function
 *
 * Bss\HelloWorld\Controller\Info
 */
class Forward extends AbstractController
{
    /**
     * @var ForwardFactory
     */
    private $forwardFactory;

    /**
     * Forward constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $forwardFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ForwardFactory $forwardFactory
    ) {
        $this->forwardFactory = $forwardFactory;
        parent::__construct(
            $context,
            $resultPageFactory
        );
    }

    /**
     * inheritdoc
     *
     * @return \Magento\Framework\Controller\Result\Forward
     */
    public function execute(): \Magento\Framework\Controller\Result\Forward
    {
        $forward = $this->forwardFactory->create();
        return $forward->forward('defaultNoRoute');
    }
}
