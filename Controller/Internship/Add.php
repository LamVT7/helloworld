<?php

namespace Bss\HelloWorld\Controller\Internship;

use Bss\HelloWorld\Model\InternData;
use Bss\HelloWorld\Model\InternDataFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action as CoreAction;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Setup\Exception;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Psr\Log\LoggerInterface;


/**
 * Class Add that save data new trainee
 *
 * Bss\HelloWorld\Controller\Internship
 */
class Add extends CoreAction
{
    /**
     * @var InternDataFactory
     */
    protected $interndata;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Add constructor.
     * @param Context $context
     * @param ResultFactory $resultFactory
     * @param InternDataFactory $trainee
     * @param MessageManagerInterface $MessageManagerInterface
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ResultFactory $resultFactory,
        InternDataFactory $InternData,
        MessageManagerInterface $MessageManagerInterface,
        LoggerInterface $logger
    ) {
        $this->resultFactory = $resultFactory;
        $this->InternData = $InternData;
        $this->messageManager = $MessageManagerInterface;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * inheritdoc
     */
    public function execute()
    {
        $resultFactory = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        /* Get request data */

        $params = $this->getRequest()->getParams();

        if (!$params || empty($params) || $params === '') {
            $this->messageManager->addWarningMessage(__('Empty request! Please specify input data!'));
            return $resultFactory->setPath('helloworld/internship/index');
        }

        $interndata = $this->InternData->create();
        try {
            /* Prepare data and save new internship */
            $interndata = $this->InternData->create();
            $data = [
                'name' => $params['name'],
                'avatar' => $params['avatar'],
                'dob' => $params['dob'],
                'description' => $params['description']
            ];
            
            $interndata->setData($data);
            $interndata->save();
            $this->messageManager->addSuccessMessage(__('Saved new internship!'));
            /* Return to index page */
            return $resultFactory->setPath('helloworld/internship/index');
        } catch (Exception $exception) {
            $this->messageManager
                ->addErrorMessage(__('Failed to save new internship!. Please check log for more information'));
            $this->logger->critical($exception->getMessage());
            return $resultFactory->setPath('helloworld/internship/index');
        }
    }
}
