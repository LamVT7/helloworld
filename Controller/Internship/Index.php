<?php

namespace Bss\HelloWorld\Controller\Internship;

use Bss\HelloWorld\Model\InternDataFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 * @package Bss\HelloWorld\Controller\Internship
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;
    protected $_interndataFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Bss\HelloWorld\Model\InternDataFactory $interndataFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_interndataFactoryFactory = $interndataFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        return $this->_pageFactory->create();
    }
}
