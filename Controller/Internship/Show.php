<?php

namespace Bss\HelloWorld\Controller\Internship;

use Bss\HelloWorld\Model\InternDataFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Show
 * @package Bss\HelloWorld\Controller\Internship
 */
class Show extends Action
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;
    protected $_interndataFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        InternDataFactory $internDataFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_interndataFactory = $internDataFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        return $this->_pageFactory->create();
    }
}
