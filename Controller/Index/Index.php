<?php
namespace Bss\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page as PageResult;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index indicate controller index action
 *
 * Bss\HelloWorld\Controller\Index
 */
class Index extends Action implements ActionInterface
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Main controller execute and return data
     *
     * @return ResponseInterface|ResultInterface|PageResult
     */
    public function execute()
    {
        return $this->_resultPageFactory->create();
    }
}
