<?php

namespace Bss\HelloWorld\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page as PageResult;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class AbstractController that provide base action
 *
 * Bss\HelloWorld\Controller
 */
abstract class AbstractController extends Action implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Main controller execute and return data
     *
     * @return ResponseInterface|ResultInterface|PageResult
     */
    public function execute()
    {
        return $this->resultPageFactory->create();
    }
}
