<?php

namespace Bss\HelloWorld\Setup;

use Exception;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Psr\Log\LoggerInterface;


class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    protected $logger;

    public function __construct(
        LoggerInterface $logger
    )
    {
        $this->logger = $logger;
    }

    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup,
                            \Magento\Framework\Setup\ModuleContextInterface $context): void
    {
        $setup->startSetup();
        /**
         * Create table 'internship'
         */
        try {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('internship'))
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Post ID Internship'
                )
                ->addColumn(
                    'name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable => false'],
                    'Name'
                )
                ->addColumn(
                    'avatar',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'URL Avatar'
                )
                ->addColumn(
                    'dob',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                    255,
                    [],
                    'Date of Birth'
                )
                ->addColumn(
                    'description',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    [],
                    'Decription'
                )
                ->setComment('Table Internship');
            $setup->getConnection()->createTable($table);
        }
        catch
            (Exception $e) {
                $this->logger->critical(__($e->getMessage()));
                throw new FrameworkException(__($e->getMessage()));
            }

        /* Now proceed to insert data */
        $setup->getConnection()->insert(
            $setup->getTable('internship'),
            [
                'name' => 'Vo Tung Lam',
                'avatar' => 'https://prnt.sc/u05fsl',
                'dob' => '10/07/1996',
                'description' => 'Come From Vinh City',
            ]
        );
        $setup->endSetup();
    }
}
