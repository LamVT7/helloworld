<?php

namespace Bss\HelloWorld\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if ($context->getVersion()
            && version_compare($context->getVersion(), '1.1.0') < 0
        ) {
            $table = $setup->getTable('internship');
            $setup->getConnection()
                    ->insertForce(
                        $table,
                        ['name' => 'Nguyen Van A',
                            'avatar' => 'https://prnt.sc/u05fsl',
                            'dob' => '24',
                            'description' => 'description'
                        ]
                    );

            $setup->getConnection()
                    ->insertForce(
                        $table,
                        ['name' => 'Nguyen Van B',
                            'avatar' => 'https://prnt.sc/u05fsl',
                            'dob' => '24',
                            'description' => 'description'
                        ]
                    );
            $setup->getConnection()
                    ->insertForce(
                        $table,
                        ['name' => 'Nguyen Van C',
                            'avatar' => 'https://prnt.sc/u05fsl',
                            'dob' => '24',
                            'description' => 'description'
                        ]
                    );
            $setup->getConnection()
                    ->insertForce(
                        $table,
                        ['name' => 'Nguyen Van D',
                            'avatar' => 'https://prnt.sc/u05fsl',
                            'dob' => '24',
                            'description' => 'description'
                        ]
                    );
            $setup->getConnection()
                    ->insertForce(
                        $table,
                        ['name' => 'Nguyen Van E',
                            'avatar' => 'https://prnt.sc/u05fsl',
                            'dob' => '24',
                            'description' => 'description'
                        ]
                    );
        }
        $setup->endSetup();
    }
}
