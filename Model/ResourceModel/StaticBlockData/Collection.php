<?php
namespace Bss\HelloWorld\Model\ResourceModel\StaticBlockData;

use Bss\HelloWorld\Model\StaticBlockData;
use Bss\HelloWorld\Model\ResourceModel\StaticBlockData as StaticBlockDataResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    /**
     * @var string
     */
    protected $_idFieldName = 'staticblock_id';

    /**
     *  Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            StaticBlockData::class,
            StaticBlockDataResourceModel::class
        );
    }
}
