<?php
namespace Bss\HelloWorld\Model\ResourceModel\InternData;

use Bss\HelloWorld\Model\InternData;
use Bss\HelloWorld\Model\ResourceModel\InternData as InternDataResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'internship_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
                     InternData::class,
                     InternDataResourceModel::class
        );
    }

}
