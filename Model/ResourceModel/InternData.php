<?php
namespace Bss\HelloWorld\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Trainee declare resource model that query database
 *
 * Bss\HelloWorld\Model\ResourceModel
 */
class Trainee extends AbstractDb
{
    public function _construct()
    {
        $this->_init('bss_internship', 'internship_id');
    }
}
