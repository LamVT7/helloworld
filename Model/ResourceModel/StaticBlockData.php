<?php
namespace Bss\HelloWorld\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class StaticBlockData extends AbstractDb
{
    public function _construct()
    {
        // TODO: Implement _construct() method.
        $this->_init('bss_staticblock', 'staticblock_id');
    }
}
