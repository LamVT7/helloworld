<?php
namespace Bss\HelloWorld\Model;

use Magento\Framework\Model\AbstractModel;

class StaticBlockData extends AbstractModel{
    public const CACHE_TAG = 'trainee_model_cache';
    public function _construct()
    {
        $this->_init(ResourceModel\StaticBlockData::class);
    }
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
