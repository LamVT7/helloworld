<?php
namespace Bss\HelloWorld\Model;

use Magento\Framework\Model\AbstractModel;

    class InternData extends AbstractModel
{
    public const CACHE_TAG = 'internship_model_cache';

    public function _construct()
    {
        $this->_init(ResourceModel\InternData::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
