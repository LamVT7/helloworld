<?php
namespace Bss\HelloWorld\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ConfigData that query needed data
 */
class ConfigData extends AbstractHelper
{
    /**
     * Get config info by param
     *
     * @param string $configName
     * @return string|mixed
     */
    public function getConfigInfo($configName)
    {
        /* If no input path return false */
        if ($configName === '' || $configName === null) {
            return 'No Data Available';
        }

        /* Dynamic return path to shorten code */
        $path = "helloworld/info/" . $configName;

        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE
        ) ?: "Empty Data";
    }
}
